import csv
import dateutil.parser
import statistics


def read_column_with_name(column_name, filename, is_print=False):
    """!
    @brief Функция извлекает столбец с заданным именем из заданного файла

    @param column_name Название столбца, который нужно считать
    @param filename Путь до файла, из которого произволится чтение
    @param is_print Параметр типа bool, который задает необходимость печати сопровождающих сообщений
                    (по умолчанию False)

    @return Содержимое столбца в виде списка

    @exception FileNotFoundError
    @exception Exception
    """

    result = []
    if is_print:
        print("=== НАЧИНАЮ ЧТЕНИЕ ===")
        print("= Читаю из файла", filename, "столбец", column_name)
    try:
        # Первичная настройка чтения: открытие файла, создание читателя
        csv_file = open(filename, encoding='UTF8')
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        labels_index = 0
        for row in csv_reader:
            if line_count == 0:
                # Смотрим на первую строку
                if column_name in row:
                    # Если есть столбец 'column_name', то запоминаем номер столбца
                    labels_index = row.index(column_name)
                else:
                    # Если такого столбца нет, то поднимаем исключение
                    raise Exception('ОШИБКА! В файле ' + filename + ' нет столбца "' + column_name + '"')
            else:
                # Проходимся по всем строкам и изымаем данные только из нужного столбца
                result.append(int(row[labels_index]))
            line_count += 1
        csv_file.close()
        if is_print:
            # Выводим результат
            print("= Чтение завершено успешно. Результат (первые 100 элементов):")
            print(result[:100], "...")
            print("=== КОНЕЦ ЧТЕНИЯ ===")
            print()
        return result
    except FileNotFoundError as err:
        print('ОШИБКА! Файл "' + err.filename + '" не существует ')
        exit()
    except Exception as err:
        print(err)


def read_only_column(filename, is_print=False):
    """!
    @brief Функция извлекает нулевой столбец из заданного файла

    @param filename Путь до файла, из которого произволится чтение
    @param is_print Параметр типа bool, который задает необходимость печати сопровождающих сообщений
                    (по умолчанию False)

    @return Содержимое столбца в виде списка

    @exception FileNotFoundError
    @exception Exception
    """

    result = []
    if is_print:
        print("=== НАЧИНАЮ ЧТЕНИЕ ===")
        print("= Читаю из файла", filename, "нулевой столбец")
    try:
        # Первичная настройка чтения: открытие файла, создание читателя
        csv_file = open(filename, encoding='UTF8')
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            # Проходимся по всем строкам и изымаем данные только из нулевого столбца
            result.append(row[0])
        csv_file.close()
        if is_print:
            # Выводим результат
            print("= Чтение завершено успешно. Результат (первые 100 элементов):")
            print(result[:100], "...")
            print("=== КОНЕЦ ЧТЕНИЯ ===")
            print()
        return result
    except FileNotFoundError as err:
        print('ОШИБКА! Файл "' + err.filename + '" не существует ')
        exit()
    except Exception as err:
        print(err)


def read_data(directory, num_of_files=3,
              new_labels_file=False,
              new_timestamps_file=False,
              new_target_file=False,
              is_print=False):

    # Задаем название файлов с метками
    labels_filenames = []
    if new_labels_file:
        labels_filenames.append(input())
    else:
        for i in range(0, num_of_files):
            labels_filenames.append('data/' + directory + '/total_log_labeled_' + str(i) + '.csv')

    # Считываем метки и их номера в глобальных файлах из файлов
    local_labels = []
    local_numbers = []

    for filename in labels_filenames:

        new_labels = read_column_with_name('labels', filename, is_print=is_print)
        local_labels += new_labels

        # Обрезаем первый элемент, т.к. это заголовок
        new_numbers = read_only_column(filename, is_print=is_print)[1:]
        local_numbers += new_numbers

        if len(new_numbers) != len(new_labels):
            print("Несоответствие размера", filename, len(new_numbers), len(new_labels))

    for i, number in enumerate(local_numbers):
        if local_numbers[i] != "":
            local_numbers[i] = int(local_numbers[i])
        else:
            print("В указании номера записи есть пустое поле")
            exit()

    # Группируем данные в пары
    pairs = []
    for i, elem in enumerate(local_labels):
        pairs.append([local_numbers[i], elem])

    # Сортируем по номеру в наборах данных
    pairs.sort(key=lambda tup: tup[0])

    # # Проверка корректности на наличие порядка и всех элементов
    # for i in range(1, len(pairs)):
    #     if pairs[i - 1][0] + 1 == pairs[i][0] == i:
    #         print(i, "OK")
    #     else:
    #         print(i, "ERROR", pairs[i - 1][0], pairs[i][0])
    # print(len(pairs))

    # формируем лист из меток
    local_labels = []
    for pair in pairs:
        local_labels.append(pair[1])

    # Задаем название файлов с временными метками и оценками опасности
    timestamps_filename = input() if new_timestamps_file else 'data/' + directory + '/total_log_timestamp.csv'
    target_filename = input() if new_target_file else 'data/' + directory + '/total_log_target.csv'

    # Считываем и обрезаем вспомогательные данные: метки и оценки опасности
    local_timestamps = read_only_column(timestamps_filename, is_print=is_print)
    local_timestamps = local_timestamps[:len(local_labels)]

    local_target = read_only_column(target_filename, is_print=is_print)
    local_target = local_target[:len(local_labels)]

    return local_labels, local_target, local_timestamps


def sort_data(labels, target, timestamps):
    """!
    @brief Функция конвертирует время во внутреннее представление и сортирует данные по времени

    @param labels Список меток кластеров
    @param timestamps Список временных меток
    @param target Список оценок опасности

    @return Пару отсортированные метки кластеров и отсортированные временные метки
    """

    # Группируем данные с лист
    data = []
    for i in range(0, len(labels)):
        data.append((labels[i], target[i], dateutil.parser.parse(timestamps[i])))

    # Сортируем по временной метке
    data.sort(key=lambda tup: tup[2])

    # Составляем листы из отсортированных данных
    sorted_data = []
    for i in range(0, 3):
        sorted_data.append([])
    for item in data:
        for i in range(0, 3):
            sorted_data[i].append(item[i])

    #      labels          target          timestamps
    return sorted_data[0], sorted_data[1], sorted_data[2]


def get_base_info_about_data(data, is_print=False):
    """!
    @brief Функция анализиует содержимое переданного списка на уникальные элементы и число их вхождений

    @param data Список, который будет анализироваться
    @param is_print Параметр типа bool, который задает необходимость печати сопровождающих сообщений
                    (по умолчанию False)

    @return Словарь, ключами которого являются уникальные элементы, а значениями - число вхождений элементов.
            Также есть ключ 'len' значением которого является число элементов в списке. Словарь отсортирован по числу
            вхождений элементов.
    """

    if is_print:
        print("=== НАЧИНАЮ АНАЛИЗ ===")
        print("= Анализирую список", data, "на уникальные элементы и число их вхождений")

    info = {}
    different_items = set(data)

    for item in different_items:
        info[item] = data.count(item)

    sorted_info = sorted(info.items(), key=lambda kv: -kv[1])

    if is_print:
        print("= Результат анализа:")
        for info in sorted_info:
            print("==", info)
        print("= Всего различных кластеров:", len(sorted_info) - 1)
        print("=== АНАЛИЗ ЗАВЕРШЕН ===")
        print()
    return sorted_info


def add_item_to_dict(item, dictionary):
    if item in dictionary.keys():
        dictionary[item] += 1
    else:
        dictionary[item] = 1
    return dictionary


def set_max(item, maximum):
    if item == maximum:
        return "!"
    else:
        return " "


def analyze_labels_and_target(labels, target):

    dict = {}

    labels_list = list(labels)
    for label in labels_list:
        dict[label] = []

    for i, label in enumerate(labels):
        dict[label].append(target[i])

    return dict


def analyze_target(labels, target, virus_score):

    print("=== НАЧИНАЮ АНАЛИЗ ОПАСНЫХ СОБЫТИЙ ===")

    info = get_base_info_about_data(labels)
    target_groped_by_labels = analyze_labels_and_target(labels, target)

    target_dictionaries = []
    # Содержит словари в следующем порядке
    # 4 - virus, mean, good, unknown
    # 2 - virus, good
    for i in range(0, 2):
        target_dictionaries.append({})

    print("= Производится подсчет")

    # <0 means unknown label
    # 0-1 are virus total scores
    # >1 means malware due to original source of file

    for i, label in enumerate(labels):
        score = target[i]
        if score >= virus_score:
            add_item_to_dict(label, target_dictionaries[0])
        else:
            add_item_to_dict(label, target_dictionaries[1])

    print("= Подсчет произведен. Результат")
    print("lab |  num  |        virus   |         good   |")

    # Для подсчета более подробной информации
    #
    # for i, label in enumerate(labels):
    #     score = float(target[i])
    #     if score > virus_score:
    #         add_item_to_dict(label, target_dictionaries[0])
    #     elif score > 0.7:
    #         add_item_to_dict(label, target_dictionaries[1])
    #     elif score >= 0.0:
    #         add_item_to_dict(label, target_dictionaries[2])
    #     else:
    #         add_item_to_dict(label, target_dictionaries[3])
    #
    # print("= Подсчет произведен. Результат")
    # print("lab |  num  |        virus   |         mean   |         good   |      unknown   |")

    result = 0

    for item in info:

        label = item[0]
        num_of_label = item[1]

        num_of_target_with_label = []
        for target_dict in target_dictionaries:
            num_of_target_with_label.append(target_dict[label] if label in target_dict.keys() else 0)

        # Подсчет процента
        per_of_target_with_label = []
        for num_of_target in num_of_target_with_label:
            per_of_target_with_label.append(int(num_of_target / num_of_label * 100))

        # Подсчет максимального элементов
        per_max = max(per_of_target_with_label)
        char_of_target_with_label = []
        for per_of_target in per_of_target_with_label:
            char_of_target_with_label.append(set_max(per_of_target, per_max))

        label_string = '{:>3}'.format(label) + " | " \
                       '{:>5}'.format(num_of_label) + " | "

        for i in range(0, len(target_dictionaries)):
            label_string += '{:>5}'.format(num_of_target_with_label[i]) + " = " + \
                            '{:>3}'.format(per_of_target_with_label[i]) + "% " + \
                            char_of_target_with_label[i] + " | "

        print(label_string, statistics.mean(target_groped_by_labels[label]))

        ideal = 1 if char_of_target_with_label[0] == "!" else 0
        result += (ideal - statistics.mean(target_groped_by_labels[label])) ** 2

    print("=== АНАЛИЗ ОПАСНЫХ СОБЫТИЙ ЗАВЕРШЕН ===")
    print()
    return result


def get_data(num_of_files=3, virus_score=0.5, is_virus_analysis=False, is_print=False):

    local_is_print = False

    if is_print:
        print("|=== НАЧИНАЮ СЧИТЫВАНИЕ ВСЕХ ДАННЫХ ===|")
        print()

    # Считываем данные из указанной в параметрах папки
    labels, target, timestamps = read_data(directory="real_data_3",
                                           num_of_files=num_of_files,
                                           is_print=local_is_print)

    # В метках есть метка -1. Увеличиваем все labels на 1, чтобы попасть в диапазон [0, n]
    for i, elem in enumerate(labels):
        labels[i] += 1

    # Сортируем данные по возрастанию временных меток
    sorted_labels, sorted_target, sorted_timestamps = sort_data(labels=labels,
                                                                target=target,
                                                                timestamps=timestamps)

    # Замена неизвестных таргетов (-2.0), заданных Пашей вручную, на безопасные
    # Замена таргетов больше 1 на 1
    for i, target in enumerate(sorted_target):
        if float(target) == -2.0:
            sorted_target[i] = 0.0
        elif float(target) > 1.0:
            sorted_target[i] = 1.0
        # elif float(target) < virus_score:
        #     sorted_target[i] = 0
        else:
            sorted_target[i] = float(sorted_target[i])

    # ЭКСПЕРИМЕНТ: Анализируем вредоносность данных для получение оценки virus_score
    #
    # my_scores = []
    # if is_virus_analysis:
    #     for i in range(0, 20):
    #         score = 0.55 + i*0.01
    #         my_scores.append((score, analyze_target(sorted_labels, sorted_target, score)))
    # for item in my_scores:
    #     print(item)
    # exit()

    if is_virus_analysis:
        analyze_target(sorted_labels, sorted_target, virus_score)

    # Анализируем число различных меток
    # local_info = get_base_info_about_data(sorted_labels, is_print=False)

    if is_print:
        print("|=== СЧИТЫВАНИЕ ВСЕХ ДАННЫХ ЗАВЕРШЕНО ===|")
        print()

    return sorted_labels, sorted_target, sorted_timestamps
