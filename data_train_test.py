import step_1_cut_sessions as cut


def create_list_with_lists(num_of_lists):
    new_list = []
    for i in range(0, num_of_lists):
        new_list.append([])
    return new_list


def generate_train_test(labels, timestamps, target, virus_score, is_print=False):

    if is_print:
        print("=== НАЧИНАЮ РАЗДЕЛЕНИЕ ДАННЫХ НА ТРЕНИРОВОЧНУЮ И ТЕСТОВУЮ ВЫБОРКУ ===")

    # Создаем листы для сохранения создаваемых выборок: 0 - train, 1 - test
    new_labels = create_list_with_lists(2)
    new_timestamps = create_list_with_lists(2)
    new_target = create_list_with_lists(2)

    # Разрезаем данные по дням, чтобы выделить несколько дней, а не рандомный набор данных
    days = cut.time_density.cut_days(timestamps, is_print=False)
    for i, day in enumerate(days):
        print(i, len(day))
    print("!!!")
    long_days, labels, target, timestamps = cut.time_density.filter_cut_days(days, labels, target, timestamps, False)
    for i, day in enumerate(long_days):
        print(i, len(day))
    # long_days = days

    # sessions, timestamps, target = cut.cut_sessions("time_density",
    #                                                       labels, timestamps, target,
    #                                                       is_print=is_print)
    # for day in sessions:
    #     print(len(day))
    # print("!!!")
    # long_days, labels, target, timestamps = cut.time_density.filter_cut_days(sessions, labels, target, timestamps, False)
    # for day in long_days:
    #     print(len(day))

    # Проходимся по выделенным дням и распределяем данные на 2 набора
    # Выбранные дни попадают в тестовую выборку

    index_record = 0
    k = 3

    for index, day in enumerate(long_days):

        if index == 2 or index == 7 or index == 8:

            # test
            new_labels[1].append(labels[index_record:index_record + len(day)])
            new_timestamps[1].append(timestamps[index_record:index_record + len(day)])
            new_target[1].append(target[index_record:index_record + len(day)])

            # new_labels[1] += labels[index_record:index_record + len(day)]
            # new_timestamps[1] += timestamps[index_record:index_record + len(day)]
            # new_target[1] += target[index_record:index_record + len(day)]

        else:

            # train
            new_labels[0] += labels[index_record:index_record + len(day)]
            new_timestamps[0] += timestamps[index_record:index_record + len(day)]
            new_target[0] += target[index_record:index_record + len(day)]

        index_record += len(day)

    # new_labels[1] = [new_labels[1]]
    # new_timestamps[1] = [new_timestamps[1]]
    # new_target[1] = [new_target[1]]

    for item in new_target[1]:
        print(len(item), item)

    # Подсчитываем долю вирусности событий

    data_set_virus_score = [0, 0]

    for score in new_target[0]:
        if score > virus_score:
            data_set_virus_score[0] += 1
    data_set_virus_score[0] /= len(new_target[0])

    num_of_test_elems = 0
    for day in new_target[1]:
        num_of_test_elems += len(day)
        for score in day:
            if score > virus_score:
                data_set_virus_score[1] += 1
    data_set_virus_score[1] /= num_of_test_elems

    if is_print:
        print("= Разделение завершено")
        print("= Результат - train:", len(new_labels[0]), "-", data_set_virus_score[0])
        print("= Результат - test :", num_of_test_elems, " -", data_set_virus_score[1])
        print("= Процент тестовой выборки относительно тренировочной:",
              int(100 * num_of_test_elems / len(new_labels[0])), "%")
        print("=== РАЗДЕЛЕНИЕ ДАННЫХ НА ТРЕНИРОВОЧНУЮ И ТЕСТОВУЮ ВЫБОРКУ ЗАВЕРШЕНО ===")
        print()

    return new_labels[0], new_timestamps[0], new_target[0], new_labels[1], new_timestamps[1], new_target[1]
