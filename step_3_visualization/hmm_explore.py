import step_3_visualization.hmm as hmm


def get_labels_for_visualization_from_sessions(sessions):

    # Получаем список меток
    labels = []
    for session in sessions:
        labels += session

    # Оборачиваем каждую метку в отдельный лист
    labels_for_visualization = []
    for label in labels:
        labels_for_visualization.append([label])

    return labels_for_visualization


def create_hmms(from_value, to_value, sessions, target, is_print=False):

    labels = get_labels_for_visualization_from_sessions(sessions)

    if is_print:
        print("==========================================")
        print("==== НАЧИНАЮ ИССЛЕДОВАНИЕ HMM-МОДЕЛЕЙ ====")
        print("==========================================")
        print()

    result = {}

    for i in range(from_value, to_value):

        model, dot = hmm.create_hmm(sessions,
                                    num_of_states=i,
                                    path_for_graph='./step_3_visualization/result/hmm_' + str(i) + '.dot',
                                    is_show_graph=True,
                                    is_print=is_print)

        prediction = hmm.predict(model, labels)
        mean_score, virus_score = hmm.analyze_prediction(prediction, target, is_print)
        # state_result = hmm.get_virus_score_for_states(prediction, target, is_print)

        model_states = set(prediction)
        model_quality = hmm.count_model_quality(model_states, virus_score, mean_score)

        print(model_quality)

        result[i] = model_quality

    if is_print:
        print("==========================================")
        print("=== ИССЛЕДОВАНИЕ HMM-МОДЕЛЕЙ ЗАВЕРШЕНО ===")
        print("==========================================")
        print()

    return result
