from hmmlearn import hmm
import numpy as np
from graphviz import Digraph


def train_model(data, n_comp):

    # length = [len(x) for x in data]
    # model = hmm.GaussianHMM(n_components=n_comp,
    #                         covariance_type="full",
    #                         random_state=100).\
    #     fit(np.concatenate(data), length)

    # maxi = -100
    # mini = 100
    length = []
    data_list = []
    print(data)
    for day in data:
        length.append(len(day))
        data_list += day
    #     for elem in day:
    #         maxi = max(maxi, elem[0])
    #         mini = min(mini, elem[0])
    # print(maxi, mini)
    # for i in range(mini, maxi + 1):
    #     for elem in data_list:
    #         if i == elem[0]:
    #             print(i, "+")
    #             break
    #     else:
    #         print(i, "---")

    model = hmm.MultinomialHMM(n_components=n_comp, random_state=100).fit(data_list, length)
    print(model.monitor_.converged)

    prob_matrix = model.transmat_

    return model, prob_matrix


def create_graph(prob_matrix, path, show_graph=False):

    edge_range = 0.1
    dot = Digraph()

    for i in range(prob_matrix.shape[0]):
        dot.node(str(i))

    for i in range(prob_matrix.shape[0]):
        for j in range(prob_matrix.shape[1]):
            if prob_matrix[i][j] > edge_range:
                dot.edge(str(i), str(j), label='{:.2f}'.format(prob_matrix[i][j]))
    dot.render(path, view=show_graph)

    return dot


def convert_for_visualization(data):

    # Перевод данных из формата:
    # [ [item, item], [item] ]
    # к формату:
    # [ [[item], [item]], [[item]] ]
    # Завертывание каждого элемента в отдельный список

    converted_data = []

    for data_elem in data:
        data_session = []
        for elem in data_elem:
            data_session.append([elem])
        converted_data.append(data_session)

    return converted_data


def create_hmm(sessions,
               num_of_states=15,
               path_for_graph='./step_3_visualization/result/hmm.dot',
               is_show_graph=False,
               is_print=False):

    sessions_for_visualization = convert_for_visualization(sessions)

    if is_print:
        print("=== НАЧИНАЮ ПОСТРОЕНИЕ СКРЫТОЙ МОДЕЛИ МАРКОВА для " + str(num_of_states) + " состояний ===")
        print("= Рассчет модели")

    model, prob_matrix = train_model(sessions_for_visualization, num_of_states)

    if is_print:
        print("= Модель рассчитана")
        print("= Строится граф")

    dot = create_graph(prob_matrix, path_for_graph, is_show_graph)

    if is_print:
        print("= Граф построен и сохранен в " + path_for_graph)
        print("=== ПОСТРОЕНИЕ ЗАВЕРШЕНО ===")
        print()

    return model, dot


def predict(model, labels):

    return model.predict(labels)


def add_value_to_item_dict_list(value, item, dict):
    if item in dict.keys():
        dict[item].append(value)
    else:
        dict[item] = [value]
    return dict


def add_value_to_item_of_dict(value, item, dict):
    if item in dict.keys():
        dict[item] += float(value)
    else:
        dict[item] = float(value)
    return dict


def get_virus_score_for_states(prediction, target, is_print=False):

    state_score = {}
    state_num = {}

    for i, label in enumerate(prediction):
        add_value_to_item_of_dict(target[i], label, state_score)
        add_value_to_item_of_dict(1, label, state_num)

    state_result = {}

    for key in state_score:
        state_result[key] = state_score[key] / state_num[key]

    sorted_state_result = sorted(state_result.items(), key=lambda kv: -kv[1])

    if is_print:
        print("=== ОЦЕНКА ОПАСНОСТИ СОСТОЯНИЙ ===")
        print(sorted_state_result)
        print("==================================")
        print()

    return sorted_state_result


def func(value, i):
    return (-1 / (50 * (float(value) - 1.02))) * (2 * i / (i + 1))


def analyze_prediction(prediction, target, virus_score, is_print=False):

    if is_print:
        print("=== НАЧИНАЮ АНАЛИЗ ОПАСНЫХ СОБЫТИЙ для предсказания " + str(prediction) + " ===")
        print("= Производится сбор оценок")

    mean = {}
    virus = {}

    # <0 means unknown label
    # 0-1 are virus total scores
    # >1 means malware due to original source of file

    for i, item in enumerate(prediction):

        if target[i] > virus_score:
            add_value_to_item_dict_list(target[i], item, virus)
        elif float(target[i]) > 0.3:
            add_value_to_item_dict_list(target[i], item, mean)

    if is_print:
        print("= Полученные средние оценки:")
        print(" ", mean)
        print("= Полученные вирусные оценки:")
        print(" ", virus)
        print("= Производится подсчет вирусных баллов")

    mean_score = {}
    virus_score = {}

    for key in virus:
        virus_score[key] = len(virus[key])

    for key in mean:
        for i, value in enumerate(mean[key]):
            # Формула: обратная гипербола, сдвинутая к единице
            if key in mean_score.keys():
                mean_score[key] += func(value, i)
            else:
                mean_score[key] = func(value, i)

        mean_score[key] /= len(mean[key])

    sorted_virus_score = sorted(virus_score.items(), key=lambda kv: -kv[1])
    sorted_mean_score = sorted(mean_score.items(), key=lambda kv: -kv[1])

    if is_print:
        print("= Баллы вирусности и средней опасности событий")
        print(" ", sorted_virus_score)
        print(" ", sorted_mean_score)
        print("=== АНАЛИЗ ОПАСНЫХ СОБЫТИЙ ЗАВЕРШЕН ===")
        print()

    return mean_score, virus_score


def count_model_quality(model_states, virus_scores, mean_scores):

    model_quality = 0

    for score in mean_scores:
        model_quality += (score - 0.55) ** 2
    model_quality /= (len(mean_scores) - 2)

    return model_quality


def get_labels_for_visualization_from_sessions(sessions):

    # Получаем список меток
    labels = []
    for session in sessions:
        labels += session

    # Оборачиваем каждую метку в отдельный лист
    labels_for_visualization = []
    for label in labels:
        labels_for_visualization.append([label])

    return labels_for_visualization


def get_states_score(prediction, target, virus_score, is_print=False):

    if is_print:
        print("=== НАЧИНАЮ ПОДСЧЕТ ВИРУСНОСТИ СОСТОЯНИЙ " + str(prediction) + " ===")
        print("= Производится сбор оценок")

    scores = {}

    # <0 means unknown label
    # 0-1 are virus total scores
    # >1 means malware due to original source of file

    for i, item in enumerate(prediction):
        add_value_to_item_dict_list(target[i], item, scores)

    # if is_print:
    #     print("= Полученные оценки по состояниям:")
    #     for key in scores:
    #         print("data.append(", scores[key], ")")

    mean_score = {}

    for key in scores:

        for i, value in enumerate(scores[key]):
            # Формула: обратная гипербола, сдвинутая к единице
            added_value = value if value > virus_score else 0.0
            if key in mean_score.keys():
                mean_score[key] += added_value
            else:
                mean_score[key] = added_value

        mean_score[key] /= len(scores[key])

    if is_print:
        print("= Баллы вирусности и средней опасности событий")
        print(" ", mean_score)
        print("=== АНАЛИЗ ОПАСНЫХ СОБЫТИЙ ЗАВЕРШЕН ===")
        print()

    return mean_score


def create_model_and_states_scores(sessions, target, num_of_states, virus_score, is_print):

    model, dot = create_hmm(sessions,
                            num_of_states=num_of_states,
                            is_show_graph=False,
                            is_print=is_print)

    labels = get_labels_for_visualization_from_sessions(sessions)
    prediction = predict(model, labels)

    states_scores = get_states_score(prediction, target, virus_score, True)

    return model, states_scores


def get_target_prediction(model, states_scores, labels):

    prediction = model.predict(get_labels_for_visualization_from_sessions([labels]))

    predicted_target = []
    for elem in prediction:
        predicted_target.append(states_scores[elem])

    return predicted_target


def get_ideal_target(target, virus_score):

    ideal_target = []
    for item in target:
        if item >= virus_score:
            ideal_target.append(1)
        else:
            ideal_target.append(0)

    return ideal_target
