import data_operations
import step_1_cut_sessions as cut
import step_2_pattern_mining as pattern_mining
import step_3_visualization.hmm as hmm
import datetime


def analyze_cut_parameters(cut_parameters, is_print=False):

    if is_print:
        print("=== АНАЛИЗИРУЕМ ПАРАМЕТРЫ, ПО КОТОРЫМ ПРОИЗОШЛО РАЗРЕЗАНИЕ ===")
        print(" ", cut_parameters)

    cut_parameters.sort()

    if is_print:
        print("= Отсортированные параметры")
        print(" ", cut_parameters)

    counted_cut_parameters = []

    if is_print:
        print("= Подсчитываем вхождения параметров")

    count_param = 1
    for i in range(1, len(cut_parameters)):
        if cut_parameters[i] == cut_parameters[i - 1]:
            count_param += 1
        else:
            counted_cut_parameters.append([cut_parameters[i - 1], count_param])
            count_param = 1
    if len(cut_parameters) > 0:
        counted_cut_parameters.append([cut_parameters[len(cut_parameters) - 1], count_param])

    counted_cut_parameters.sort(key=lambda kv: -kv[1])

    if is_print:
        print("= Результат подсчета")
        print(" ", counted_cut_parameters)
        print("=== АНАЛИЗА ПАРАМЕТРОВ ЗАВЕРШЕН ===")
        print()

    return counted_cut_parameters


def check_existing(list, param):
    for item in list:
        if len(item[0]) > len(param):
            if item[0][:len(param)] == param:
                return True
        else:
            if item[0] == param[:len(item[0])]:
                return True
    return False


def get_long_cut_parameters(counted_cut_parameters, length=1, freq=1, is_print=False):

    if is_print:
        print("=== НАЧИНАЮ ОТБОР ПАРАМЕТРОВ ДЛЯ РАЗРЕЗАНИЯ с частотой более", freq, "и длинной более", length)
        print("= Водные данные:", counted_cut_parameters)

    counted_cut_parameters.sort(key=lambda tup: -tup[1])

    long_cut_parameters = []

    for param in counted_cut_parameters:
        if len(param[0]) > length and param[1] > freq:
            if not check_existing(long_cut_parameters, param[0]):
                long_cut_parameters.append(param)

    if is_print:
        print("= Результат")
        print(" ", long_cut_parameters)
        print("=== ОТБОР ЗАВЕРШЕН ===")
        print()

    return long_cut_parameters


def get_labels_list(info):

    labels_set = set()

    for item in info:
        if item[0] != 'len' and item[0] > 0:
            labels_set.add(item[0])

    return list(labels_set)


def replace_new_stations(old_sessions, old_timestamps, old_target, new_stations, num_stations):

    # Проходимся по всем сессиям
    new_sessions = []
    new_target = []
    new_timestamps = []

    global_index = 0

    for session in old_sessions:

        insert_indexes = []
        what_insert_indexes = []
        i = 0
        # Ищем индексы для замены
        while i < len(session):

            index_of_cut_station = 0
            cut_param = []
            while index_of_cut_station < len(new_stations):
                cut_param = new_stations[index_of_cut_station]
                if session[i:i + len(cut_param)] == cut_param:
                    break
                else:
                    index_of_cut_station += 1

            if index_of_cut_station < len(new_stations):
                insert_indexes.append(i)
                what_insert_indexes.append(index_of_cut_station)
                i += len(cut_param)
            else:
                i += 1

        if insert_indexes:
            # Заменяем
            index_of_session = 0
            index_of_insert = 0
            new_session = []

            while index_of_session < len(session):
                if index_of_insert == len(insert_indexes) or index_of_session != insert_indexes[index_of_insert]:

                    new_session.append(session[index_of_session])

                    curr_global_index = global_index + index_of_session
                    new_target.append(old_target[curr_global_index])
                    new_timestamps.append(old_timestamps[curr_global_index])
                    index_of_session += 1

                else:
                    what_replace = new_stations[what_insert_indexes[index_of_insert]]
                    what_insert = num_stations[what_insert_indexes[index_of_insert]]
                    new_session.append(what_insert)

                    new_target.append(
                        max(old_target[global_index + index_of_session:
                                       global_index + index_of_session + len(what_replace)]))

                    new_timestamps.append(
                        min(old_timestamps[global_index + index_of_session:
                                           global_index + index_of_session + len(what_replace)]))

                    index_of_insert += 1
                    index_of_session += len(what_replace)
            new_sessions.append(new_session)
        else:
            new_sessions.append(session)
            new_target += old_target[global_index: global_index + len(session)]
            new_timestamps += old_timestamps[global_index: global_index + len(session)]

        global_index += len(session)

    return new_sessions, new_target, new_timestamps


def add_new_stations_after_cut(old_sessions, old_target, old_timestamps,
                               cut_parameters, global_new_stations, is_print=False):

    if is_print:
        print("= Выделяю новые состояния")

    labels_set = set()
    for session in old_sessions:
        labels_set |= set(session)

    pre_labels_list = list(labels_set)

    labels_list = []
    for label in pre_labels_list:
        if label > 0:
            labels_list.append(label)

    # Делаю создание новых состояний по часто-встретившимся параметрам

    # Создаем лист из новых состояний
    new_stations = []
    for param_info in cut_parameters:
        if len(param_info[0]) > 1:
            new_stations.append(param_info[0])

    # Создаем нумерацию для новых состояний
    num_stations = []
    first_num_station = labels_list[len(labels_list) - 1] + 1
    for i in range(0, len(new_stations)):
        num_stations.append(first_num_station)
        first_num_station += 1

    stations_and_numerations = []
    for i in range(0, len(new_stations)):
        stations_and_numerations.append([new_stations[i], num_stations[i]])
    global_new_stations += stations_and_numerations

    if is_print:
        print("= Добавлены состояния:", stations_and_numerations)

    new_sessions, new_target, new_timestamps = replace_new_stations(old_sessions, old_timestamps, old_target,
                                                                    new_stations, num_stations)

    if is_print:
        print("= Выделение новых состояний завершено")
        print()

    return new_sessions, new_target, new_timestamps


def get_labels_from_sessions(sessions):
    labels = []
    for session in sessions:
        labels += session
    return labels


def get_num_of_states(sessions):
    data = []
    for session in sessions:
        data += session
    result = data_operations.get_base_info_about_data(data, False)
    labels = []
    for elem in result:
        label, value = elem
        if label != 'len':
            labels += [label]
    labels.sort()
    return labels[len(labels) - 1] // 4


def generate_model(scenario,
                   labels, timestamps, target,
                   global_new_stations,
                   virus_score,
                   is_print=False):

    # ===========================================
    # === ЭТАП 1: Разрезаем сессии по времени ===
    # ===========================================

    time_cut = scenario // 10000

    target_1 = target.copy()
    sessions_1, timestamps_1 = [], []

    if time_cut == 1:

        sessions_1, timestamps_1 = [labels.copy()], timestamps.copy()

    elif time_cut == 2:

        sessions_1, timestamps_1, target_1 = cut.cut_sessions("time_density",
                                                              labels, timestamps, target,
                                                              is_print=is_print)

    # ==========================================================
    # === ЭТАП 2: Делаем дополнительное разрезание на сессии ===
    # ==========================================================

    local_is_print = True

    extra_time_cut = scenario % 10000 // 1000
    sessions_2, cut_parameters_2 = [], []
    target_2, timestamps_2 = target_1.copy(), timestamps_1.copy()

    if extra_time_cut == 1:

        sessions_2 = sessions_1.copy()
        cut_parameters_2 = []

    elif extra_time_cut == 2:

        print("Метод Suffix Tree не реализован. Попробуйте использовать другой метод")
        exit()

        # for i, time_session in enumerate(sessions_1):
        #     # Проверить параметры метода + допилить автоматизм
        #     params_for_cut, small_sessions = cut_sessions("suffix_tree", 100, is_print=main_is_print)
        #     cut_parameters += params_for_cut
        #     sessions_2 += small_sessions

    elif extra_time_cut == 3:

        for i, time_session in enumerate(sessions_1):

            params_for_cut, small_sessions = cut.normaltest.cut(time_session, is_cut=True, is_print=local_is_print)

            sessions_2 += small_sessions
            cut_parameters_2 += params_for_cut

    elif extra_time_cut == 4:

        # Собираем параметры, по которым можно произвести разрезание
        for i, time_session in enumerate(sessions_1):
            params_for_cut, small_sessions = cut.normaltest.cut(time_session, is_cut=False, is_print=local_is_print)
            cut_parameters_2 += params_for_cut

        # Анализируем параметры: делаем посчет и отсекаем редкие правила
        counted_cut_parameters = analyze_cut_parameters(cut_parameters_2, is_print=True)
        if time_cut == 1:
            freq = 0
        else:
            freq = max(len(sessions_1) // 5, 5)
        long_cut_parameters = get_long_cut_parameters(counted_cut_parameters, length=0, freq=freq, is_print=True)

        cut_parameters_2 = long_cut_parameters.copy()

        # Переводим параметры в формат, необходимый для работы метода
        only_cut_parameters = []
        for cut_info in long_cut_parameters:
            only_cut_parameters.append(cut_info[0])

        # Выполняем разрезание
        for session in sessions_1:
            sessions_2 += cut.normaltest.cut_sessions(only_cut_parameters, session)

    elif extra_time_cut == 5:

        # Собираем параметры, по которым можно произвести разрезание
        for i, time_session in enumerate(sessions_1):
            params_for_cut, small_sessions = cut.normaltest.cut(time_session, is_cut=False, is_print=local_is_print)
            cut_parameters_2 += params_for_cut

        # Анализируем параметры: делаем посчет и отсекаем редкие правила
        counted_cut_parameters = analyze_cut_parameters(cut_parameters_2, is_print=True)
        if time_cut == 1:
            freq = 0
        else:
            freq = 1  # Возможно стоит поставить 5, а не 4!
        long_cut_parameters = get_long_cut_parameters(counted_cut_parameters, length=1, freq=freq, is_print=True)

        cut_parameters_2 = long_cut_parameters.copy()
        sessions_2 = sessions_1.copy()

    # Для просмотра промежуточных результатов
    # mean_1 = 0
    # for s in sessions_1:
    #     mean_1 += len(s)
    # mean_1 /= len(sessions_1)
    #
    # mean_2 = 0
    # for s in sessions_2:
    #     mean_2 += len(s)
    # mean_2 /= len(sessions_2)
    #
    # print("!", len(sessions_1), mean_1)
    # print("!", len(sessions_2), mean_2)

    # ===============================================================
    # === ЭТАП 3: Выделяем частые параметры в отдельные состояния ===
    # ===============================================================

    new_stations_after_cut = scenario % 1000 // 100
    sessions_3, target_3, timestamps_3 = [], [], []

    if new_stations_after_cut == 1:

        sessions_3, target_3, timestamps_3 = sessions_2.copy(), target_2.copy(), timestamps_2.copy()

    elif new_stations_after_cut == 2:

        print("ДОБАВЛЯЮ НОВЫЕ СОСТОЯНИЯ")

        if extra_time_cut == 4 or extra_time_cut == 5:

            sessions_3, target_3, timestamps_3 = add_new_stations_after_cut(sessions_2, target_2, timestamps_2,
                                                                            cut_parameters_2, global_new_stations,
                                                                            is_print=is_print)

        else:

            # ТАКОГО СЦЕНАРИЯ НЕ БУДЕТ
            # Анализируем параметры, по которым произошло разрезание
            counted_cut_parameters = analyze_cut_parameters(cut_parameters_2, is_print=True)
            # freq = 0 if time_cut == 1 else 1
            # long_cut_parameters = get_long_cut_parameters(counted_cut_parameters, freq=freq, is_print=True)
            #
            # sessions_3, target_3, timestamps_3 = add_new_stations_after_cut(sessions_2, target_2, timestamps_2,
            #                                                                 long_cut_parameters, global_new_stations,
            #                                                                 is_print=is_print)

    # =========================================
    # === ЭТАП 4: Используем Pattern Mining ===
    # =========================================

    pattern_mining_algorithm = scenario % 100 // 10
    sessions_4, target_4, timestamps_4 = sessions_3.copy(), target_3.copy(), timestamps_3.copy()
    rules = []

    if pattern_mining_algorithm == 1:

        rules = []

    elif pattern_mining_algorithm == 2:

        print("Метод Apriori не реализован. Попробуйте использовать другой метод")
        exit()

        # rules = pattern_mining.find_rules("apriori", sessions=sessions_3,
        #                                   is_print=is_print)

    elif pattern_mining_algorithm == 3:

        rules = pattern_mining.find_rules("prefixspan", sessions=sessions_3,
                                          freq=max(len(sessions_3) // 4, 25), is_print=is_print)

        format_rules = []
        for rule in rules:
            format_rules.append([rule[1], rule[0]])

        # Анализируем параметры, по которым произошло разрезание
        freq = len(sessions_3) // 3
        long_cut_parameters = get_long_cut_parameters(format_rules, freq=freq, length=1, is_print=True)

        sessions_4, target_4, timestamps_4 = add_new_stations_after_cut(sessions_3, target_3, timestamps_3,
                                                                        long_cut_parameters, global_new_stations,
                                                                        is_print=is_print)

    elif pattern_mining_algorithm == 4:

        rules = pattern_mining.find_rules("mowcatl", labels=labels,
                                          is_print=is_print)

    # ============================
    # === ЭТАП 5: Визуализация ===
    # ============================

    is_visualization = scenario % 10

    if is_visualization == 0:

        # hmm_explore.create_hmms(5, 15, sessions_4, target_4, is_print=False)

        num_of_states = get_num_of_states(sessions_4)
        model, states_scores = hmm.create_model_and_states_scores(sessions_4, target_4, num_of_states, virus_score, is_print)

        return model, states_scores

    else:

        return sessions_4, target_4, timestamps_4
