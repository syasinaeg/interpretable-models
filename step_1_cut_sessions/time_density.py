
def cut_days(timestamps, is_print=False):

    if is_print:
        print("= Начинаю разрезание по дням")

    days = []
    local_day = [timestamps[0]]

    for i in range(1, len(timestamps)):
        if (timestamps[i] - timestamps[i - 1]).days > 0:
            days.append(local_day)
            local_day = [timestamps[i]]
        else:
            local_day.append(timestamps[i])

    days.append(local_day)

    if is_print:
        print("= Разрезание по дням завершено. Всего дней:", len(days))
        for i, day in enumerate(days):
            print("==", '{:>3}'.format(i), "|", '{:>5}'.format(len(day)), "|", day[0], "...", day[len(day) - 1])

    return days


def filter_cut_days(days, labels, target, timestamps, is_print=False):

    local_labels = labels.copy()
    local_target = target.copy()
    local_timestamps = timestamps.copy()

    if is_print:
        print("= Начинаю удаление дней менее 100 событий")

    index_of_record = 0
    index_of_day = 0

    while index_of_day < len(days):
        dlina = len(days[index_of_day])
        if dlina < 100:
            days.pop(index_of_day)
            for i in range(0, dlina):
                del local_labels[index_of_record]
                del local_target[index_of_record]
                del local_timestamps[index_of_record]

        else:
            index_of_record += len(days[index_of_day])
            index_of_day += 1

    if is_print:
        print("= После удаления коротких дней")
        for i, day in enumerate(days):
            print("==", '{:>3}'.format(i), "|", '{:>5}'.format(len(day)), "|", day[0], "...", day[len(day) - 1])

    return days, local_labels, local_target, local_timestamps


def get_long_sessions(sessions, timestamps, target, min_seconds, is_print=False):

    if is_print:
        print("= Отбрасываю СЛИШКОМ короткие сессиии - менее", min_seconds, "секунд и менее 100 событий")

    long_sessions = []
    long_sessions_timestamps = []
    long_target = []

    target_index = 0
    for i, session in enumerate(sessions):
        duration = timestamps[i][len(timestamps[i]) - 1] - timestamps[i][0]
        if duration.total_seconds() >= min_seconds and len(timestamps[i]) >= 100:
            long_sessions.append(session)
            long_sessions_timestamps.append(timestamps[i])
            long_target += target[target_index: target_index + len(session)]
        target_index += len(session)

    if is_print:
        print("= Отброшено", len(sessions) - len(long_sessions), "сессий")
        print("= Разрезание на сессии завершено. Результат -", len(long_sessions), "сессий:")
        for i, session in enumerate(long_sessions):
            duration = long_sessions_timestamps[i][len(long_sessions_timestamps[i]) - 1] - \
                       long_sessions_timestamps[i][0]
            print("== ", '{:>5}'.format(len(session)), "|",
                  long_sessions_timestamps[i][0], "...",
                  long_sessions_timestamps[i][len(long_sessions_timestamps[i]) - 1],
                  "|", duration)

    return long_sessions, long_sessions_timestamps, long_target


def cut(labels, timestamps, target, is_filter, is_print):

    # Делим на дни

    if is_print:
        print("=== НАЧИНАЮ РАЗРЕЗАНИЕ ПО ПЛОТНОСТИ СОБЫТИЙ ===")

    days = cut_days(timestamps, is_print)

    if is_filter:
        long_days, labels, target, timestamps = filter_cut_days(days, labels, target, timestamps, is_print)
    else:
        long_days, labels, target, timestamps = days, labels, target, timestamps

    # Подсчитываем плотность в каждом из дней

    if is_print:
        print("= Начинаю построение плотности событий по каждому дню")

    cut_places = []
    shift = 0

    for num_of_day, day in enumerate(long_days):

        # day - лист из временных меток

        cut_places.append(shift)

        # Нормальзуем время относительно начальной точки в дне и переведем в пятиминутные интервалы

        # Начальная точка дня
        start_time = day[0]
        # Нормализованные временные метки
        normal_times = []

        for time in day:
            normal_time = (time - start_time).total_seconds() // 60 // 5
            # Создается список из списков, которые являются элементами
            # Это необходимо для работы плотности
            normal_times.append([normal_time])

        # Находим плотность

        from sklearn.neighbors import KernelDensity
        import numpy as np

        left_border = int(normal_times[len(normal_times) - 1][0])

        if left_border == 0:
            shift += len(day)
            if is_print:
                print("== Для дня", num_of_day, "разрезаний нет")
            continue

        # Обучение модели
        kde = KernelDensity(kernel='gaussian', bandwidth=0.75).fit(normal_times)
        x_plot = np.linspace(0, left_border, left_border * 10)[:, np.newaxis]
        log_dens = kde.score_samples(x_plot)

        # Перевод плотности в числовые значения
        density = []
        for elem in log_dens:
            density.append(np.exp(elem))

        # Обработка полученных значений плотности

        # Создание хитрого списка, показывающего как делить на сессии
        session_labels = []
        for i in range(0, left_border + 1):
            # Метка -2 означает, что этот диапазон еще не был рассмотрен
            session_labels.append(-2)

        # Просчет мест, в которых плотность очень маленькая
        index = 0
        while index < len(density):
            result = False
            for i in range(0, 10):
                result = result or (density[index] < 500 * (10 ** (-10)))
                # Было вот так
                # result = result or (density[index] < 500 * (10 ** (-6)))
                index += 1
            if result:
                # Метка -1 означает, что в этом диапазоне плотность очень маленькая
                session_labels[(index // 10) - 1] = -1

        # Индексация мест с нормальной плотностью
        index_of_session = 0
        for i, elem in enumerate(session_labels):
            if elem == -2:
                session_labels[i] = index_of_session
            elif elem == -1 and session_labels[i - 1] != -1:
                index_of_session += 1

        # Проход по нормализованным временным меткам дня

        # Индекс локальной последовательности
        last_seq_index = 0
        for i, time in enumerate(normal_times):
            if session_labels[int(time[0])] != last_seq_index:
                cut_places.append(i + shift)
                last_seq_index = session_labels[int(time[0])]

        if is_print:
            print("== Для дня", num_of_day, "произошло разрезание")

        shift += len(day)

    if is_print:
        print("= Построение плотностей произошло. Необходимо разрезать метки в следующих", len(cut_places), "местах")
        print("==", cut_places)

    if is_print:
        print("= Происходит разрезание на сессии")

    sessions = []
    session_timestamps = []

    for i in range(1, len(cut_places)):
        elem = labels[cut_places[i - 1]:cut_places[i]]
        if elem:
            sessions.append(elem)
            session_timestamps.append(timestamps[cut_places[i - 1]:cut_places[i]])

    sessions.append(labels[cut_places[len(cut_places) - 1]:])
    session_timestamps.append(timestamps[cut_places[len(cut_places) - 1]:])

    if is_filter:

        min_seconds = 60
        long_sessions, long_sessions_timestamps, long_target = get_long_sessions(sessions,
                                                                                 session_timestamps,
                                                                                 target,
                                                                                 min_seconds,
                                                                                 is_print=is_print)
    else:

        long_sessions, long_sessions_timestamps, long_target = sessions, session_timestamps, target

    if is_print:
        print("=== РАЗРЕЗАНИЕ ПО ПЛОТНОСТИ СОБЫТИЙ ЗАВЕРШЕНО ===")
        print()

    final_session_timestamps = []
    for lst in long_sessions_timestamps:
        final_session_timestamps += lst

    return [long_sessions, final_session_timestamps, long_target]
