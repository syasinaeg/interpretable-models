import step_1_cut_sessions.suffix_tree
import step_1_cut_sessions.suffix_tree_explore
import step_1_cut_sessions.time
import step_1_cut_sessions.time_density
import step_1_cut_sessions.normaltest


def cut_sessions(method, local_labels, local_timestamps=[], local_target=[],
                 num_of_sessions=0, quick=False, is_filter=False, is_print=False):

    if method == "time":
        local_sessions = time.cut(local_labels, local_timestamps, num_of_sessions, is_print=is_print)

    elif method == "time_density":
        local_sessions = time_density.cut(local_labels, local_timestamps, local_target,
                                          is_filter=is_filter, is_print=is_print)

    elif method == "suffix_tree":
        if quick:
            local_sessions = suffix_tree.quick_cut(num_of_sessions, is_print=is_print)
        else:
            local_sessions = suffix_tree.cut(local_labels, num_of_sessions, is_print=is_print)

    elif method == "suffix_tree_explore":
        # Исследование! Второй параметр - желаемое число сессий. Алгоритм прогоняется на всех возможных параметрах
        # Далее полученные данные анализируюся в Jupyter Notebook
        suffix_tree_explore.explore(local_labels)
        local_sessions = "Explore"

    elif method == "normaltest":
        local_sessions = normaltest.cut(local_labels, is_print=is_print)

    else:
        print("Ошибка! Такой метод разделения на сессии не реализован")
        exit()

    return local_sessions
