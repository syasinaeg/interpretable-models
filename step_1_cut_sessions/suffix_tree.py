import sys
import plotly.plotly as py
import plotly.graph_objs as go
from igraph import *

numeration = 0
options = []


class Tree:
    def __init__(self):
        self.children = []
        self.data = None
        self.count = 0
        self.cut = 0

    def return_child_with_data(self, data):
        # Метод класса, который проверяет есть ли у объекта потомок с полем data = @param'data'
        # Возвращаемое значение - искомый потомок при удачном поиске, False - иначе
        for child in self.children:
            if child.data == data:
                return child
        return False

    def print(self, level=0, max_level=10000):
        # Функция печатает содержимое объекта на экран
        # Глубина дерева при выводе не превышает @param'max_level'
        # Возвращаемое значение отсутствует
        if level <= max_level:
            # В начале каждой строки печатается уровень
            print(level, sep="", end="")
            pos = 0
            level_copy = level
            while level_copy > 0:
                pos += 1
                level_copy //= 10
            for i in range(pos, level):
                print("-", sep="", end="")
            # Далее печатается содержимое узла и число его вхождений
            print("({0}, {1})".format(self.data, self.count))
            for child in self.children:
                child.print(level+1, max_level)

    def count_vertices(self, level=0, max_level=10000):
        # Для красивого вывода
        if not self.children:
            return 1
        else:
            child_vertices = 0
            if level < max_level:
                for child in self.children:
                    child_vertices += child.count_vertices(level+1, max_level)
            return child_vertices + 1

    def return_edges(self, num=0, level=0, max_level=10000):
        # Для красивого вывода
        global numeration
        edges = []
        if not self.children:
            return []
        else:
            if level < max_level:
                for child in self.children:
                    numeration += 1
                    num_child = numeration
                    edges += [(num, num_child)]
                    edges += child.return_edges(num_child, level+1, max_level)
            return edges

    def create_labels(self, level=0, max_level=10000):
        # Для красивого вывода
        labels = [str(self.data) + ": " + str(self.count)]
        if not self.children:
            return labels
        else:
            if level < max_level:
                for child in self.children:
                    labels += child.create_labels(level+1, max_level)
            return labels

    def print_advanced(self, max_level=10000):
        # TODO Реализовать графический вывод
        global numeration

        # Создаем граф во внутреннем представлении
        numeration = 0
        vertices_num = self.count_vertices(max_level=max_level)
        edges = self.return_edges(num=numeration, max_level=max_level)
        graph = Graph()
        graph.add_vertices(vertices_num)
        graph.add_edges(edges)

        # Указываем то, как будет отображаться граф
        # Попробовать несколько вариантов:
        # drl, lgl, tree
        lay = graph.layout('tree')

        vertices_labels = self.create_labels(max_level=max_level)

        # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        position = {k: lay[k] for k in range(vertices_num)}
        Y = [lay[k][1] for k in range(vertices_num)]
        M = max(Y)

        es = EdgeSeq(graph)  # sequence of edges
        E = [e.tuple for e in graph.es]  # list of edges

        L = len(position)
        Xn = [position[k][0] for k in range(L)]
        Yn = [2 * M - position[k][1] for k in range(L)]
        Xe = []
        Ye = []
        for edge in E:
            Xe += [position[edge[0]][0], position[edge[1]][0], None]
            Ye += [2 * M - position[edge[0]][1], 2 * M - position[edge[1]][1], None]

        labels = vertices_labels

        # ----------------------------------

        lines = go.Scatter(x=Xe,
                           y=Ye,
                           mode='lines',
                           line=dict(color='rgb(210,210,210)', width=1),
                           hoverinfo='none'
                           )
        dots = go.Scatter(x=Xn,
                          y=Yn,
                          mode='markers',
                          name='',
                          marker=dict(symbol='dot',
                                      size=18,
                                      color='#6175c1',  # '#DB4551',
                                      line=dict(color='rgb(50,50,50)', width=1)
                                      ),
                          text=labels,
                          hoverinfo='text',
                          opacity=0.8
                          )

        # -----------------------------------

        def make_annotations(pos, text, font_size=10, font_color='rgb(250,250,250)'):
            L = len(pos)
            if len(text) != L:
                raise ValueError('The lists pos and text must have the same len')
            annotations = go.Annotations()
            for k in range(L):
                annotations.append(
                    go.Annotation(
                        text=labels[k],  # or replace labels with a different list for the text within the circle
                        x=pos[k][0], y=2 * M - position[k][1],
                        xref='x1', yref='y1',
                        font=dict(color=font_color, size=font_size),
                        showarrow=False)
                )
            return annotations

        # ------------------------------------------------------

        axis = dict(showline=False,  # hide axis line, grid, ticklabels and  title
                    zeroline=False,
                    showgrid=False,
                    showticklabels=False,
                    )

        layout = dict(title='Tree with Reingold-Tilford Layout',
                      annotations=make_annotations(position, v_label),
                      font=dict(size=12),
                      showlegend=False,
                      xaxis=go.XAxis(axis),
                      yaxis=go.YAxis(axis),
                      margin=dict(l=40, r=40, b=85, t=100),
                      hovermode='closest',
                      plot_bgcolor='rgb(248,248,248)'
                      )

        # -------------------------------

        data = go.Data([lines, dots])
        fig = dict(data=data, layout=layout)
        fig['layout'].update(annotations=make_annotations(position, v_label))
        py.offline.plot(fig, filename='Suffix-Tree', auto_open=True)


def add_labels_to_tree(labels, tree, depth):
    # Рекурсивная функция, которая заполняет @param'tree' в соответствие с @param'labels'
    # Возвращаемого значения нет - результат сохраняется в @param'tree'

    # !!! Глубина сильно играет на быстродействие
    if depth < 7:
        if len(labels) > 0:
            first_label = labels[0]
            child_with_data = tree.return_child_with_data(first_label)
            if not child_with_data:
                # Если потомок не нашелся, то:
                # - создаем новый узел с меткой и счетчиком в 1
                new_tree = Tree()
                new_tree.data = first_label
                new_tree.count = 1
                # - добавляем его к родителю
                tree.children.append(new_tree)
                # - продолжаем формирование дерева в нем
                add_labels_to_tree(labels[1:], new_tree, depth + 1)
            else:
                # Если потомок нашелся, то увеличиваем его счетчик и продолжаем формирование дерева
                child_with_data.count += 1
                add_labels_to_tree(labels[1:], child_with_data, depth + 1)


def fill(tree, labels):
    # Функция воссоздает по @param'labels' суффиксное дерево и записывает его в @param'tree'
    # Возвращаемого значения нет - результат сохраняется в @param'tree'
    for index, label in enumerate(labels):
        # Создание суффиксов через срезы
        add_labels_to_tree(labels[index:], tree, 0)


def create_suffix_tree(labels):
    # Функция создает суффиксное дерево по @param'labels'
    # Возвращаемое значение - полученное суффиксное дерево
    tree = Tree()
    tree.data = "ROOT"
    tree.count = len(labels)
    fill(tree, labels)
    return tree


def fill_cut_option(tree, labels, seq=[]):
    # Для каждого узла дерева подсчитывается на сколько сессий реально делит последовательность
    seq.append(tree.data)
    if tree.data == 'ROOT':
        tree.cut = tree.count
    else:
        cut_option = [seq]
        tree.cut = len(cut_sessions(cut_option, labels.copy()))
    for child in tree.children:
        fill_cut_option(child, labels.copy(), seq.copy())


def find_cut_options(tree, param, seq=[]):
    # Функция, которая находит в @param'tree' первые по обходу дерева в глубину последовательности меток длины
    # меньшей чем @param'param'
    # Возвращаемое значение отсутствует - результат (возможные последовательности меток и число таких вхождений)
    # сохраняются в глобальный список options
    global options
    if tree.data == 'ROOT':
        options = []
    if tree.cut <= param:
        seq.append(tree.data)
        options.append((seq.copy(), tree.cut))
    else:
        seq.append(tree.data)
        for child in tree.children:
            find_cut_options(child, param, seq.copy())


def best_cut_option():
    # Функция, находящая в листе options элемент с наибольшим числом вхождений
    options.sort(key=lambda tup: -tup[1])
    # TODO Сделать осознанный выбор, если есть несколько вариантов совокупности меток с одинаковой частотой
    return options[0]


def cut_sessions(option, labels):
    # Функция, разрезающая @param'labels' на сессии по @param'option'
    # Возвращаемое значение - получившиеся сессии
    cut_seq = option[0][1:]
    cut_len = len(cut_seq)
    # Находим индексы, по которым поделим последовательность
    cut_indexes = []
    i = 0
    while i < len(labels):
        if labels[i:i+cut_len] == cut_seq:
            cut_indexes.append(i)
            i += cut_len
        else:
            i += 1
    # Разрезаем на сессии по найденным индексам
    sessions = []
    prev_i = 0
    for i in cut_indexes:
        session = labels[prev_i:i]
        if session:
            sessions.append(labels[prev_i:i])
        prev_i = i
        i += cut_len
    sessions.append(labels[prev_i:])
    return sessions


def cut(labels, num_of_sessions, suffix_tree="Empty", is_print=False):
    # Функция разрезает @param'labels' на @param'num_of_sessions' сессий методом модифицированного суффиксного дерева
    # Возвращаемое значение - лист из полученных сессий

    global options
    options = []

    if is_print:
        print("=== НАЧИНАЮ РАЗДЕЛЕНИЕ НА СЕССИИ методом суффиксного дерева ===")

    # Команда для увеличения максимальной глубины стека
    # sys.setrecursionlimit(2000)

    if suffix_tree == "Empty":

        # Создаем суффиксное дерево по последовательности меток
        if is_print:
            print("= Формирую суффиксное дерево")

        suffix_tree = create_suffix_tree(labels)

        if is_print:
            max_level = 10
            print("= Формирование завершено. Результат - с точностью до уровня", max_level)
            suffix_tree.print(max_level=max_level)
            # suffix_tree.print_advanced(max_level=2)

        if is_print:
            print("= Считаю реальное число сессий, по которым произойдет разрез")

        fill_cut_option(suffix_tree, labels, [])

        if is_print:
            print("= Рассчет завершен")

    # Находим наиболее частые вхождения, которые делят последовательность на нужное num_of_sessions фрагментов
    if is_print:
        print("= Ищу параметры для разделения на сессии")

    find_cut_options(suffix_tree, num_of_sessions, [])

    if is_print:
        print("= Поиск завершен. Возможные варианты параметров для разрезания:")
        for seq in options:
            print(seq)

    # Находим лучший вариант для разрезания
    if is_print:
        print("= Поиск лучшего параметра для разрезания")

    cut_option = best_cut_option()

    if is_print:
        print("= Поиск завершен. Лучший параметр для разрезания", options[0][0], "с частотой", options[0][1])

    # Разрезаем последовательность меток на сессии в соответствии с cut_options
    if is_print:
        print("= Выполняю разрезание на сессии по параметру", cut_option[0][1:])

    sessions = cut_sessions(cut_option, labels.copy())

    if is_print:
        print("= Разрезание выполнено. Получившиеся сессии:")
        for session in sessions:
            print(len(session), session)

    if is_print:
        print("=== РАЗДЕЛЕНИЕ НА СЕССИИ завершено ===")

    return sessions.copy()


def quick_cut(num_of_sessions, is_print=True):
    # Данный метод можно использовать, если заранее в файлы были сгененрированы все возможные разрезания
    # с помощью функции ... . Тогда из нужного файла из нужной строки считается нужное разделение

    sessions = []

    # TODO Добавить поясняющие сообщения для вывода

    # Формируем имя файла для чтения
    file_index = num_of_sessions // 100 + 1
    line_index = num_of_sessions % 100
    if file_index > 1:
        line_index += 1
    filename = "explore_"
    if file_index < 10:
        filename += "0"
    filename += str(file_index) + ".txt"

    directory = "suffix_tree_explore_v_1_2"

    try:
        # Читаем разрезание на сессии с рассчитанной строки
        with open("step_1_cut_sessions/" + directory + "/" + filename, "r") as file:
            i = 1
            for line in file:
                if i == line_index:
                    sessions = eval(line)
                    break
                else:
                    i += 1
        # Возвращаем считанную строку
        return sessions

    except FileNotFoundError as err:
        print('ОШИБКА! Файл "' + err.filename + '" не существует. Попробуте использовать метод "suffix_tree_explore"')
        exit()
