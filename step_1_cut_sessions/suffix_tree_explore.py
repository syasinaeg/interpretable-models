import step_1_cut_sessions.suffix_tree as suffix_tree


def explore(labels):
    """
        Функция извлекает нулевой столбец из заданного файла.

        Args:
            filename:      Путь до файла, из которого произволится чтение.
            is_print:      Параметр типа bool, который задает необходимость печати сопровождающих сообщений.
                           По умолчанию False.

        Returns:
            Содержимое столбца в виде списка.

        Raises:
            FileNotFoundError, Exception.
    """

    index = 1

    tree = suffix_tree.create_suffix_tree(labels)
    suffix_tree.fill_cut_option(tree, labels, [])

    filename = "explore_01.txt"
    file = open(filename, "w+")
    print("= Created file '", filename, "'")

    for i in range(1, len(labels)):

        if index < i // 100 + 1:
            file.close()
            print("= Closed file '", filename, "'")

            index = i // 100 + 1
            filename = "explore_"
            if index < 10:
                filename += "0"
            filename += str(index) + ".txt"

            file = open(filename, "w+")
            print("= Created file '", filename, "'")

        print("Explore for", i, "from", len(labels))
        sessions = suffix_tree.cut(labels.copy(), i, suffix_tree=tree)
        file.write(str(sessions) + "\n")

    file.close()
