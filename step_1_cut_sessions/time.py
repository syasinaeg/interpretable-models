import datetime
import dateutil.parser


def cut(labels, timestamps, num_of_sessions, is_print):
    # TODO Добавить поясняющий вывод
    inner_delta = []

    for i in range(0, len(timestamps) - 1):
        inner_delta.append((i, timestamps[i + 1] - timestamps[i]))

    inner_delta.sort(key=lambda tup: -tup[1])

    inner_delta = inner_delta[:num_of_sessions]
    inner_delta.sort(key=lambda tup: tup[0])

    sessions = []

    if inner_delta[0][0] != 0:
        sessions.append(labels[inner_delta[0][0] : inner_delta[1][0]])

    for i in range(0, len(inner_delta) - 1):
        sessions.append(labels[inner_delta[i][0] : inner_delta[i + 1][0]])

    if inner_delta[len(inner_delta) - 1][0] != len(labels) - 1:
        sessions.append(labels[inner_delta[len(inner_delta) - 1][0] : len(labels) - 1])

    return sessions
