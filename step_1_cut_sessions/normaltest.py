from scipy import stats


class STreeNode:
    def __init__(self, key):
        self.key = key
        self.child_dict = {}
        self.start_indexes = []

    def __hash__(self):
        return self.key.__hash__(self)

    @property
    def occurrence(self):
        return len(self.start_indexes)

    def __repr__(self):
        return self.key

    def __str__(self, level=0):
        ret = "\t" * level + repr(self.key) + "\n"
        for key in self.child_dict:
            ret += self.child_dict[key].__str__(level=level + 1)
        return ret

    def filter_on_min_occurrence(self, min_occurrence):
        drop_keys = []
        for key in self.child_dict:
            if self.child_dict[key].occurrence < min_occurrence:
                drop_keys.append(key)
            else:
                self.child_dict[key].filter_on_min_occurrence(min_occurrence)

        for key in drop_keys:
            del self.child_dict[key]

        return self

    def filter_on_normaltest(self, min_p):
        drop_keys = []
        for key in self.child_dict:
            if self.child_dict[key].occurrence < 8:
                drop_keys.append(key)
                continue
            k2, p = stats.normaltest(self.child_dict[key].start_indexes)
            if p < min_p:
                drop_keys.append(key)
            else:
                self.child_dict[key].filter_on_normaltest(min_p)

        for key in drop_keys:
            del self.child_dict[key]

        return self


def get_stree(cluster_markers, max_depth=10, starting_symbol='^'):
    if max_depth <= 10 or len(cluster_markers) <= 15:
        return "None"
    else:
        head = STreeNode(starting_symbol)
        cluster_markers = ['^'] + list(cluster_markers)
        for i in range(1, len(cluster_markers)):
            sublist = list(cluster_markers[i:i+max_depth])
            parent = head
            head.start_indexes.append(i-1)
            for offset, key in enumerate(sublist):
                if key not in parent.child_dict:
                    parent.child_dict[key] = STreeNode(key)

                parent.child_dict[key].start_indexes.append(i+offset-1)
                parent = parent.child_dict[key]

        return head


parameters = []


def get_cut_parameters(tree, seq=[]):
    global parameters
    if tree.key == '^':
        parameters = []
    if tree.child_dict == {}:
        seq.append(tree.key)
        if seq != ['^']:
            parameters.append(seq.copy())
    else:
        seq.append(tree.key)
        for key in tree.child_dict:
            get_cut_parameters(tree.child_dict[key], seq.copy())


def cut_sessions(options, labels):
    # Функция, разрезающая @param'labels' на сессии по @param'option'
    # Возвращаемое значение - получившиеся сессии

    # Находим индексы, по которым поделим последовательность
    cut_indexes = []
    i = 0
    while i < len(labels):
        for option in options:
            cut_seq = option
            cut_len = len(option)
            if labels[i:i+cut_len] == cut_seq:
                cut_indexes.append(i)
                i += cut_len
                break
        else:
            i += 1
    # Разрезаем на сессии по найденным индексам
    sessions = []
    prev_i = 0
    for i in cut_indexes:
        session = labels[prev_i:i]
        if session:
            sessions.append(labels[prev_i:i])
        prev_i = i
        i += cut_len
    sessions.append(labels[prev_i:])

    return sessions


def cut(labels, is_cut=True, is_print=False):
    global parameters
    parameters = []

    if is_print:
        print("=== НАЧИНАЮ РАЗРЕЗАНИЕ НА СЕССИИ ПО НОРМАЛЬНОМУ ПОВТОРЕНИЮ ===")
        print()
        print("= Анализизую для:")
        print(" ", labels)

    # Перевод всех значений int в char для работы алгоритма
    char_labels = []
    for label in labels:
        char_labels.append(str(label))

    if is_print:
        print("= Строю дерево")

    stree_head = get_stree(char_labels, max_depth=20)

    if stree_head == "None":

        if is_print:
            print("= Не получилось построить дерево. Возвращаю результат без изменений")
            print("=== РАЗРЕЗАНИЕ НА СЕССИИ ПО НОРМАЛЬНОМУ ПОВТОРЕНИЮ ЗАВЕРШЕНО ===")
            print()
        return [[], [labels.copy()]]

    else:

        if is_print:
            print("= Фильтрую дерево")

        filter_stree_head = stree_head.filter_on_min_occurrence(min_occurrence=8).filter_on_normaltest(0.1)

        if is_print:
            print("= Полученное дерево")
            print(filter_stree_head)
            print("= Получаю параметры для разрезания")

        get_cut_parameters(filter_stree_head, [])

        if is_print:
            print("= Полученные параметры")
            print(" ", parameters)

        new_parameters = []

        if len(parameters) > 0:

            if is_print:
                print("= Делю по выявленным параметрам")

            for param in parameters:
                new_param = []
                for i in range(1, len(param)):
                    new_param.append(int(param[i]))
                new_parameters.append(new_param)

            if is_cut:
                sessions = cut_sessions(new_parameters, labels)
            else:
                sessions = labels.copy()

        else:

            if is_print:
                print("= Параметры не найдены - возвращаю, что было на входе")
            sessions = [labels.copy()]

        if is_print:
            print("= Полученные сессии")
            print(" ", sessions)
            print("=== РАЗРЕЗАНИЕ НА СЕССИИ ПО НОРМАЛЬНОМУ ПОВТОРЕНИЮ ЗАВЕРШЕНО ===")
            print()

        return [new_parameters, sessions]
