import data_operations
import data_train_test
import algorithm
import step_1_cut_sessions as cut
import step_3_visualization.hmm as hmm


def add_new_stations_to_test_data(test_labels, test_timestamps, test_target, global_new_stations):

    new_stations = []
    num_stations = []
    for elem in global_new_stations:
        new_stations.append(elem[0])
        num_stations.append(elem[1])

    new_labels, new_target, new_timestamps = algorithm.replace_new_stations(
        [test_labels], test_timestamps, test_target, new_stations, num_stations)
    new_labels = new_labels[0]

    print(test_labels)
    print(new_labels)

    return new_labels, new_timestamps, new_target


def analyze_labels_and_target(labels, target):

    dict = {}

    labels_list = list(labels)
    for label in labels_list:
        dict[label] = []

    for i, label in enumerate(labels):
        dict[label].append(target[i])

    print(dict)
    exit()


def convert_to_old_scenario(scenario):
    time = scenario // 10
    stations = scenario % 10
    result = 0
    if time == 0:
        if stations == 0:
            result = 11110
        elif stations == 2:
            result = 14110
        elif stations == 3:
            result = 15210
    elif time == 1:
        if stations == 0:
            result = 21110
        elif stations == 1:
            result = 23110
        elif stations == 2:
            result = 24110
        elif stations == 3:
            result = 25210
    return result


def get_accuracy(ideal, prediction, virus_score):

    binary_prediction = hmm.get_ideal_target(prediction, virus_score)
    accuracy = 0
    for i in range(0, len(ideal)):
        if ideal[i] == binary_prediction[i]:
            accuracy += 1
    return accuracy / len(ideal)


def get_far_and_frr(ideal, prediction, virus_score):
    good = 0
    bad = 0
    is_bad_but_good = 0
    is_good_but_bad = 0
    binary_prediction = hmm.get_ideal_target(prediction, virus_score)
    for i in range(0, len(ideal)):
        if ideal[i] == 0:
            good += 1
        else:
            bad += 1
        if ideal[i] == 1 and binary_prediction[i] == 0:
            is_bad_but_good += 1
        if ideal[i] == 0 and binary_prediction[i] == 1:
            is_good_but_bad += 1
    if bad != 0:
        far = is_bad_but_good / bad
    else:
        far = is_bad_but_good * -100000

    if good != 0:
        frr = is_good_but_bad / good
    else:
        frr = is_good_but_bad * -100000
    return far, frr


def get_mediums(result):
    mediums = []
    for elem in result:
        print(elem)
        local_medium = 0
        for item in elem[1]:
            local_medium += item
        mediums.append(local_medium/len(elem[1]))
    return mediums


def get_global_medium(mediums):
    global_medium = 0
    for item in mediums:
        global_medium += item
    return global_medium / len(mediums)


if __name__ == '__main__':

    # Выбор СТАРОГО сценария - пятизначное число

    # _==== - необходимость разрезания по TimeDensity
    #         1 - не нужно
    #         2 - нужно

    # =_=== - алгоритм для разрезания на сессии
    #         1 - не нужно
    #         // не автоматичен // 2 - SuffixTree
    #         3 - NormalTest - каждая сессия разрезается на подсессии отдельно от других
    #         4 - NormalTest - собираются параметры, по которым могло бы произойти разрезание,
    #                          выделяются частые и происходит разрезание по ним
    #         5 - NormalTest - собираются параметры, по которым могло бы произойти разрезание,
    #                          выделяются частые, однако разрезания НЕ происходит

    # ==_== - выделяем частые параметры для разрезания в отдельные состояния?
    #         1 - нет
    #         2 - да

    # ===_= - алгоритм PatternMining
    #         1 - не нужно
    #         // не работает // 2 - Apriori
    #         3 - PrefixSpan
    #         // ассоциативные правила не дают какую-то оценку // 4 - MOWCATL

    virus_score = 0.3
    main_is_print = True

    # Старый список сценариев
    # scenarios = [
    #     11110, 13110, 14110, 14210, 15210,
    #     11130, 13130, 14130, 14230, 15230,
    #     21110, 23110, 24110, 24210, 25210,
    #     21130, 23130, 24130, 24230, 25230,
    # ]

    scenarios = [
        0,       2,  3,
        10, 11, 12, 13
    ]
    # Проверка сценариев
    #


    # OK
    labels, target, timestamps = data_operations.get_data(num_of_files=3,
                                                          virus_score=virus_score,
                                                          is_virus_analysis=False,
                                                          is_print=main_is_print)

    # Метод для получения оценок по кластерам - словарь с вирусными оценками
    # analyze_labels_and_target(labels, target)

    train_labels, train_timestamps, train_target, \
        test_labels, test_timestamps, test_target = \
        data_train_test.generate_train_test(labels=labels,
                                            timestamps=timestamps,
                                            target=target,
                                            virus_score=virus_score,
                                            is_print=main_is_print)

    ac_results = []
    far_results = []
    frr_results = []
    roc_results = []

    for new_scenario in scenarios:

        scenario = convert_to_old_scenario(new_scenario)

        print(new_scenario, scenario)

        global_new_stations = []

        local_train_labels = train_labels.copy()
        local_train_timestamps = train_timestamps.copy()
        local_train_target = train_target.copy()

        model, states_scores = algorithm.generate_model(scenario,
                                                        local_train_labels,
                                                        local_train_timestamps,
                                                        local_train_target,
                                                        global_new_stations,
                                                        virus_score=virus_score,
                                                        is_print=main_is_print)

        local_test_labels = test_labels.copy()
        local_test_timestamps = test_timestamps.copy()
        local_test_target = test_target.copy()

        local_ac_results = []
        local_far_results = []
        local_frr_results = []
        local_roc_results = []

        for j, day in enumerate(local_test_labels):

            # if scenario // 10000 == 2:
            #
            # new_test_sessions, pre_new_test_timestamps, pre_new_test_target = cut.cut_sessions("time_density",
            #                                                                                    local_test_labels[j],
            #                                                                                    local_test_timestamps[j],
            #                                                                                    local_test_target[j],
            #                                                                                    is_filter=False,
            #                                                                                    is_print=True)
            #
            # new_test_timestamps = []
            # new_test_target = []
            #
            # iterator = 0
            # for session in new_test_sessions:
            #     new_test_timestamps.append(pre_new_test_timestamps[iterator:iterator+len(session)])
            #     new_test_target.append(pre_new_test_target[iterator:iterator + len(session)])
            #     iterator += len(session)
            #
            # print(new_test_sessions)
            # print(new_test_timestamps)
            # print(new_test_target)
            #
            # else:

            new_test_sessions, new_test_timestamps, new_test_target = [local_test_labels[j]],\
                                                                      [local_test_timestamps[j]],\
                                                                      [local_test_target[j]]

            for i, small_day in enumerate(new_test_sessions):

                if global_new_stations:
                    new_test_sessions[i], new_test_timestamps[i], new_test_target[i] = \
                        add_new_stations_to_test_data(new_test_sessions[i],
                                                      new_test_timestamps[i],
                                                      new_test_target[i],
                                                      global_new_stations)

                predicted_target = hmm.get_target_prediction(model, states_scores, new_test_sessions[i])
                ideal_target = hmm.get_ideal_target(new_test_target[i], virus_score)

                accuracy = get_accuracy(ideal_target, predicted_target, virus_score)
                print(scenario, "- ACCURACY:", accuracy)
                local_ac_results += [accuracy]

                far, frr = get_far_and_frr(ideal_target, predicted_target, virus_score)
                print(scenario, "- FAR:", far)
                print(scenario, "- FRR:", frr)
                local_far_results += [far]
                local_frr_results += [frr]

                from sklearn.metrics import roc_auc_score
                try:

                    final_score = roc_auc_score(ideal_target, predicted_target)
                    # print("ROC-AUC:", final_score)

                    print(scenario, "- ROC-AUC:", final_score)
                    local_roc_results += [final_score]

                except ValueError:
                    print(scenario, "- ROC-AUC:", -1)
                    local_roc_results += [-1]

                print()

        ac_results.append([scenario, local_ac_results])
        far_results.append([scenario, local_far_results])
        frr_results.append([scenario, local_frr_results])
        roc_results.append([scenario, local_roc_results])
        print()

    med_ac_res = get_mediums(ac_results)
    med_far_res = get_mediums(far_results)
    med_frr_res = get_mediums(frr_results)
    med_roc_res = get_mediums(roc_results)

    med_ac = get_global_medium(med_ac_res)
    med_far = get_global_medium(med_far_res)
    med_frr = get_global_medium(med_frr_res)
    med_roc = get_global_medium(med_roc_res)

    print()
    print("ACCURACY", med_ac)
    print("FAR", med_far)
    print("FRR", med_frr)
    print("ROC-AUC", med_roc)
