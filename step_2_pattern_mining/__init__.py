import step_2_pattern_mining.myapriori
import step_2_pattern_mining.myapriori_explore
import step_2_pattern_mining.myprefixspan
import step_2_pattern_mining.myprefixspan_explore
import step_2_pattern_mining.mymowcatl
import step_2_pattern_mining.mymowcatl_explore

# Apriori


def apriori_sessions(labels, is_print=True):
    return myapriori.find_rules([labels], is_print)


def apriori_sequence(sessions, is_print=True):
    return myapriori.find_rules(sessions, is_print)


def apriori_explore(labels, indexes):
    myapriori_explore.explore(labels, indexes)

# PrefixSpan


def prefixspan_sessions(sessions, frequency, is_print=True):
    return myprefixspan.find_rules(sessions, frequency, is_print)


def prefixspan_sequence(labels, frequency, is_print=True):
    return myprefixspan.find_rules([labels], frequency, is_print)


def prefixspan_explore(labels, indexes):
    myprefixspan_explore.explore(labels, indexes)

# Mowcatl


def mowcatl(labels, is_print=True):
    return mymowcatl.mowcatl(labels, labels, 0.1, 0.1, 4, 4, 1, is_print)
    # mymowcatl.mowcatl(labels_char, labels_char, 0.25, 0.5, 3, 3, 1)


def mowcatl_explore(labels):
    return mymowcatl_explore.explore(labels)


def convert_to_char_list(list):
    char_list = []
    for item in list:
        char_list.append(str(item))
    return char_list


def find_rules(method, labels=[], sessions=[], freq=10, is_print=False):
    """
    Функция для поиска правил
    """

    if method == "apriori":

        rules = apriori_sequence(sessions, is_print=is_print)
        # rules = pattern_mining.apriori_sessions(labels, is_print=is_print)

    elif method == "apriori_explore":

        # pattern_mining.apriori_explore(labels, unique_indexes)
        rules = "Explore"

    elif method == "prefixspan":

        rules = prefixspan_sessions(sessions, freq, is_print=is_print)

    elif method == "prefixspan_explore":

        # pattern_mining.prefixspan_explore(labels, unique_indexes)
        rules = "Explore"

    elif method == "mowcatl":

        rules = mowcatl(labels, is_print=is_print)

    elif method == "mowcatl_explore":

        mowcatl_explore(labels)
        rules = "Explore"

    else:
        print("Ошибка! Такой метод поиска правил не реализован")
        exit()

    return rules
