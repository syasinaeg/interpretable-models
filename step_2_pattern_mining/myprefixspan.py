from prefixspan import PrefixSpan

# https://pypi.org/project/prefixspan/


def find_rules(sessions, frequency, is_print=True):

    if is_print:
        print("=== НАЧИНАЮ ВЫДЕЛЕНИЕ МОДЕЛЕЙ методом PrefixSpan ===")
        print("= Данные:", sessions)

    ps = PrefixSpan(sessions)

    if is_print:
        print("= Общий алгоритм отработал")
        print("= Начинаю поиск частых правил с частотой", frequency)

    frequent_rules = ps.frequent(frequency, generator=True)

    if is_print:
        print("= Частые правила выделены")
    # frequent_rules.sort(key=lambda tup: -len(tup[1]))
    if is_print:
        print("= Правила отсортированы")

    if is_print:
        print(len(frequent_rules), "правил с частотой больше", frequency, ":", frequent_rules)

    # print(ps.topk(10, closed=True, generator=True))
    return frequent_rules
