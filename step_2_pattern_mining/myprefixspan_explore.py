import step_1_cut_sessions as cut
import step_2_pattern_mining as pattern_mining

# Интересные наблюдения
# 1. Алгоритм срабатывает не при всех разрезаниях на сессии, если просить его сгенерировать правила небольшой длины.
#    Наименьший параметр, при котором алгоритм справился - 13


def explore(labels, indexes):

    filename = "prefixspan_explore_v_1.txt"
    file = open(filename, "w+")

    for i in indexes:

        print("Do for", i)
        sessions = cut.by_suffix_tree(labels, i, is_print=False, quick=True)
        rules_prefixspan = pattern_mining.prefixspan_sessions(sessions, 12, is_print=False)
        file.write(str(i) + "\n" + str(rules_prefixspan) + "\n")

    file.close()
