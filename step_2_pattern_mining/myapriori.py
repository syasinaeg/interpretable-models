from apyori import apriori

# https://pypi.org/project/apyori/


def print_rules(association_results, detailed=False):
    print("= Выделено", len(association_results), "правил")
    for i, item in enumerate(association_results):
        # first index of the inner list
        # Contains base item and add item
        pair = item[0]
        items = [x for x in pair]
        if len(items) == 1:
            print("Rule: " + str(items[0]))
        else:
            print("Rule: " + str(items[0]) + " -> " + str(items[1]))

        if detailed:
            # second index of the inner list
            print("Support: " + str(item[1]))

            # third index of the list located at 0th
            # of the third index of the inner list

            print("Confidence: " + str(item[2][0][2]))
            print("Lift: " + str(item[2][0][3]))
            print("===")


def convert_to_list(association_results):
    list = []
    for i, item in enumerate(association_results):
        # first index of the inner list
        # Contains base item and add item
        pair = item[0]
        items = [x for x in pair]
        if len(items) == 1:
            list_item = [items[0]]
        else:
            list_item = [items[0], items[1]]
        list.append(list_item)
    return list


def print_list(list):
    if list:
        elem = list[0][0]
    for item in list:
        if item[0] != elem:
            print()
            elem = item[0]
        print(item, end=" ")
    print()


def find_rules(records, is_print=True):
    # https://stackabuse.com/association-rule-mining-via-apriori-algorithm-in-python/
    if is_print:
        print("=== НАЧИНАЮ ВЫДЕЛЕНИЕ МОДЕЛЕЙ методом Apriori ===")
        print("= Данные:", records)
    association_results = list(apriori(records, min_support=0.01, min_confidence=0.01, min_lift=1, min_length=1))
    if is_print:
        print_rules(association_results, detailed=True)

    rules_in_list = convert_to_list(association_results)
    rules_in_list.sort()
    if is_print:
        print_list(rules_in_list)

    return rules_in_list
