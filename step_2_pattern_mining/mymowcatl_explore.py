import step_2_pattern_mining as pattern_mining


def explore(labels):

    filename = "mowcatl_explore_v_2.txt"
    file = open(filename, "w+")

    for i in range(1, 6):

        result = pattern_mining.mymowcatl.mowcatl(labels, labels, 0.1, 0.1, i, i, 1, is_print=False)
        file.write(str(result) + "\n")

    file.close()
