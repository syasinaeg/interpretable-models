import numpy as np


def flatten(l):
    return [item for sublist in l for item in sublist]


def count_includes2(sequence, subsequence, max_window_size):
    min_occur = []
    for i in range(0, len(sequence) - max_window_size+1):
        subcounter = 0
        start = -1
        for j in range(0, max_window_size):
            if sequence[i+j] == subsequence[subcounter]:
                subcounter += 1
                if start == -1:
                    start = i+j
            if subcounter >= len(subsequence):
                stop = i+j
                if [start+1, stop+1] not in min_occur:
                    min_occur.append([start+1, stop+1])
                break

    return min_occur


# check if set is subset
def check_contains(outer_list, inner_list):
    outer_list = list(outer_list)
    inner_list = list(inner_list)
    if len(inner_list) == 0:
        return False

    while len(inner_list) > 0:
        if inner_list[0] not in outer_list:
            return False

        outer_list.pop(outer_list.index(inner_list[0]))
        inner_list.pop(0)

    return True


def count_includes(sequence, subsequence, max_window_size):
    min_occur = []
    subsequence = list(subsequence)
    for i in range(0, len(sequence) - max_window_size+1):
        if check_contains(sequence[i:i+max_window_size], subsequence):
            start = i
            stop = i+max_window_size
            while check_contains(sequence[start:stop], subsequence):
                start += 1
            start -= 1

            while check_contains(sequence[start:stop], subsequence):
                stop -= 1
            stop += 1

            if [start+1, stop] not in min_occur:
                min_occur.append([start+1, stop])

    return min_occur


def gen_bigger_subsequence(subsequence):
    new_subsequence = []
    set_box = []
    for i in subsequence:
        for j in np.unique(flatten(subsequence)):
            tmp = list(i)
            tmp.append(j)

            if tmp not in new_subsequence and not set(tmp) in set_box:
                new_subsequence.append(tmp)
                set_box.append(set(tmp))
    return new_subsequence


def mowcatl(sequence_a, sequence_c, min_sup, min_conf, win_a, win_c, lag, is_print=True):
    # initializing
    ate = np.unique(sequence_a)
    cte = np.unique(sequence_c)

    new_ate = []
    ate_occurs_all = []
    ate_all = []
    ate_support_all = []
    for i in ate:
        min_occurs = count_includes(sequence_a, [i], 1)
        support = len(min_occurs) / len(sequence_a)
        if support >= min_sup:
            new_ate.append([i])
            ate_all.append([i])
            ate_occurs_all.append(min_occurs)
            ate_support_all.append(support)
    ate = new_ate

    new_cte = []
    cte_occurs_all = []
    cte_all = []
    cte_support_all = []
    for i in cte:
        min_occurs = count_includes(sequence_c, [i], 1)
        support = len(min_occurs) / len(sequence_c)
        if support >= min_sup:
            new_cte.append([i])
            cte_all.append([i])
            cte_occurs_all.append(min_occurs)
            cte_support_all.append(support)
    cte = new_cte

    # algo itself
    while len(ate) != 0:
        # generate ate candidates
        new_ate = gen_bigger_subsequence(ate)

        # filter ate candidates on support
        ate = []
        for i in new_ate:
            min_occurs = count_includes(sequence_a, i, win_a)
            support = len(min_occurs) / len(sequence_a)
            if support >= min_sup:
                ate.append(i)
                ate_all.append(i)
                ate_occurs_all.append(min_occurs)
                ate_support_all.append(support)

    while len(cte) != 0:
        # generate cte candidates
        new_cte = gen_bigger_subsequence(cte)

        # filter cte candidates on support
        cte = []
        for i in new_cte:
            min_occurs = count_includes(sequence_c, i, win_c)
            support = len(min_occurs) / len(sequence_c)
            if support >= min_sup:
                cte.append(i)
                cte_all.append(i)
                cte_occurs_all.append(min_occurs)
                cte_support_all.append(support)

    result = []

    # generate rules
    for ate, ate_occures, ate_support in zip(ate_all, ate_occurs_all, ate_support_all):
        for cte, cte_occures, cte_support in zip(cte_all, cte_occurs_all, cte_support_all):
            counter = 0

            for a_occures in ate_occures:
                for c_occures in cte_occures:
                    if c_occures[0] - a_occures[1] == lag:
                        counter += 1

            if counter / len(ate_occures) >= min_conf:
                if list(np.unique(ate)) != list(np.unique(cte)):
                    if is_print:
                        print("{}=>{} with confidence {}; ate_support {}; cte_support {}".format(
                        ate, cte, counter / len(ate_occures), ate_support, cte_support))
                    elem = [ate, cte, counter / len(ate_occures), ate_support, cte_support]
                    result.append(elem)

    return result
